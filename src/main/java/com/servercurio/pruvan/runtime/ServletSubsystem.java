package com.servercurio.pruvan.runtime;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.servercurio.pruvan.servlet.RequestServlet;
import org.eclipse.jetty.http.CookieCompliance;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.servlet.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServletSubsystem implements Subsystem {

	private static final Logger log = LoggerFactory.getLogger(ServletSubsystem.class);
	private Server jettyServer;
	private Connector httpConnector;

	private volatile ObjectMapper objectMapper;

	public ServletSubsystem() {

	}

	@Override
	public void configure(final String[] args) {

		objectMapper = new ObjectMapper();
		jettyServer = new Server();

		final HttpConfiguration httpConfiguration = new HttpConfiguration();
		httpConfiguration.setCookieCompliance(CookieCompliance.RFC6265);
		httpConfiguration.setOutputBufferSize(32768);

		httpConfiguration.setSendXPoweredBy(false);
		httpConfiguration.setSendDateHeader(true);
		httpConfiguration.setSendServerVersion(false);

		final ServerConnector httpConnector = new ServerConnector(jettyServer, new HttpConnectionFactory(httpConfiguration));
		httpConnector.setPort(8080);
		jettyServer.addConnector(httpConnector);

		final ServletHandler servletHandler = new ServletHandler();
		servletHandler.addServletWithMapping(RequestServlet.class, "/");
		jettyServer.setHandler(servletHandler);

		try {
			jettyServer.start();
		} catch (Exception ex) {
			log.error(LoggingSubsystem.LOGM_EXCEPTION, "Error Initializing Servlet System");
			Runtime.getInstance().stop();
		}

		log.info(LoggingSubsystem.LOGM_STARTUP, "Initialized Servlet System");
	}

	@Override
	public void shutdown() {
		try {
			jettyServer.stop();
		} catch (Exception ex) {
			log.warn(LoggingSubsystem.LOGM_EXCEPTION, "Servlet Shutdown Interrupted");
		}

		log.info(LoggingSubsystem.LOGM_STARTUP, "Shutdown Servlet System");
	}

	public ObjectMapper getObjectMapper() {
		return objectMapper;
	}
}
