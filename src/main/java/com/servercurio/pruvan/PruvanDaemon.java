package com.servercurio.pruvan;

import com.servercurio.pruvan.runtime.LoggingSubsystem;
import com.servercurio.pruvan.runtime.Runtime;
import org.apache.commons.daemon.*;

public class PruvanDaemon implements Daemon {

	public PruvanDaemon() {

	}

	@Override
	public void init(final DaemonContext daemonContext) throws DaemonInitException, Exception {
		LoggingSubsystem.getInstance().configure(daemonContext.getArguments());
		Runtime.getInstance().configure(daemonContext.getArguments());
	}

	@Override
	public void start() throws Exception {
		Runtime.getInstance().start();
	}

	@Override
	public void stop() throws Exception {
		Runtime.getInstance().stop();
	}

	@Override
	public void destroy() {
		Runtime.getInstance().destroy();
	}

}
