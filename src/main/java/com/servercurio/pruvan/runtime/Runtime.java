package com.servercurio.pruvan.runtime;

import com.servercurio.pruvan.selenium.NewUserAutomation;
import com.servercurio.pruvan.servlet.*;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

public final class Runtime {

	private static final Runtime instance = new Runtime();
	private static final Logger log = LoggerFactory.getLogger(Runtime.class);

	private static final int WORKER_WAIT_INTERVAL = 5000;

	private Thread runtimeThread;
	private ServletSubsystem servletSubsystem;
	private PropertySubsystem propertySubsystem;
	private SeleniumSubsystem seleniumSubsystem;

	private ExecutorService workerExecutors;

	private Runtime() {
	}

	public static Runtime getInstance() {
		return instance;
	}

	public void configure(final String[] args) {
		log.info(LoggingSubsystem.LOGM_STARTUP, "Configuring System");

		propertySubsystem = new PropertySubsystem();
		propertySubsystem.configure(args);

		servletSubsystem = new ServletSubsystem();
		servletSubsystem.configure(args);

		seleniumSubsystem = new SeleniumSubsystem();
		seleniumSubsystem.configure(args);

		runtimeThread = new Thread(this::worker);
		runtimeThread.setName("< runtime worker >");

		workerExecutors = Executors.newScheduledThreadPool(5);

		java.lang.Runtime.getRuntime().addShutdownHook(new Thread(this::destroy));
	}

	public void start() {
		log.info(LoggingSubsystem.LOGM_STARTUP, "Starting System Worker");
		runtimeThread.start();
	}

	public void stop() {
		log.info(LoggingSubsystem.LOGM_STARTUP, "Shutting Down System Worker");

		try {
			workerExecutors.shutdown();
			servletSubsystem.shutdown();
			propertySubsystem.shutdown();

			runtimeThread.interrupt();
			runtimeThread.join();
			workerExecutors.awaitTermination(1000, TimeUnit.MILLISECONDS);

			log.info(LoggingSubsystem.LOGM_STARTUP, "Stopped System Worker");
		} catch (InterruptedException ex) {
			log.warn(LoggingSubsystem.LOGM_EXCEPTION, "System Worker Shutdown Interrupted", ex);
		}
	}

	public void join() throws InterruptedException {
		runtimeThread.join();
	}

	public void destroy() {
		if (workerExecutors != null) {
			if (!workerExecutors.isShutdown()) {
				workerExecutors.shutdownNow();
			}

			workerExecutors = null;
		}

		if (seleniumSubsystem != null) {
			seleniumSubsystem.shutdown();
			seleniumSubsystem = null;
		}

		if (servletSubsystem != null) {
			servletSubsystem.shutdown();
			servletSubsystem = null;
		}

		if (propertySubsystem != null) {
			propertySubsystem.shutdown();
			propertySubsystem = null;
		}

		if (runtimeThread != null) {
			if (runtimeThread.isAlive() && !runtimeThread.isInterrupted()) {
				runtimeThread.interrupt();
			}

			runtimeThread = null;
		}

		log.info(LoggingSubsystem.LOGM_STARTUP, "System Shutdown Hook Executed");
	}

	public ServletSubsystem getServletSubsystem() {
		return servletSubsystem;
	}

	@SuppressWarnings("unchecked")
	public Future<PruvanUserResponse> enqueue(final PruvanUserRequest request) {
		return workerExecutors.submit(() -> {
			final WebDriver webDriver = seleniumSubsystem.create();

			try (NewUserAutomation automation = new NewUserAutomation(webDriver)){
				return automation.execute(request);
			} catch (Exception ex) {
				log.error(LoggingSubsystem.LOGM_SELENIUM, "Automation Error", ex);
				return new PruvanUserResponse(PruvanUserStatus.FAILED_EXCEPTION);
			}
		});
	}

	private void worker() {

		try {
			while (true) {
				Thread.sleep(WORKER_WAIT_INTERVAL);
			}
		} catch (InterruptedException ex) {
			// Suppress this interrupted exception
		}

	}

}
