package com.servercurio.pruvan.selenium;

import com.servercurio.pruvan.runtime.ServletSubsystem;
import com.servercurio.pruvan.servlet.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class NewUserAutomation implements AutoCloseable {

	private static final Logger log = LoggerFactory.getLogger(ServletSubsystem.class);

	private static final String URL_PRUVAN_LOGIN = "https://northsight.direct.pruvan.com/v2/login";
	private static final String URL_PRUVAN_DASHBOARD = "https://northsight.direct.pruvan.com/v2/pmgr";
	private static final String URL_PRUVAN_USER_MGMT = "https://northsight.direct.pruvan.com/v2/users";

	private static final String FLD_LOGIN_USER_NAME = "username";
	private static final String FLD_LOGIN_PASSWORD = "password";
	private static final String BTN_LOGIN = "submit";
	private static final String LBL_LOGIN_ERROR = "result";

	private static final String FLD_USER_SEARCH = "search";
	private static final String TBL_USERS = "users";
	private static final String BTN_NEW_USER = "new";

	private static final String WND_NEW_USER = "usersModal";
	private static final String FLD_USER_USERNAME = "username";
	private static final String FLD_USER_PASSWORD = "password";
	private static final String FLD_USER_FIRST_NAME = "firstName";
	private static final String FLD_USER_LAST_NAME = "lastName";
	private static final String FLD_USER_EMAIL_ADDRESS = "email";
	private static final String FLD_USER_ROLE = "roleSelect";
	private static final String FLD_USER_CHECK_IN_NUMBER = "check_in_number";
	private static final String BTN_USER_SAVE = "save";
	private static final String LBL_USER_ERROR = "usersModalAlertGrp";

	private WebDriver webDriver;

	public NewUserAutomation(final WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	public WebDriver getWebDriver() {
		return webDriver;
	}

	public PruvanUserResponse execute(final PruvanUserRequest request) {

		final ChromeDriver chromeDriver = (ChromeDriver) webDriver;

		final WebDriverWait wait = new WebDriverWait(chromeDriver, 20);
		final Actions builder = new Actions(chromeDriver);

		chromeDriver.manage().window().fullscreen();
		chromeDriver.navigate().to(URL_PRUVAN_LOGIN);
		wait.until(urlContains(URL_PRUVAN_LOGIN));

		// Locate login fields
		wait.until(and(presenceOfElementLocated(By.id(FLD_LOGIN_USER_NAME)),
				presenceOfElementLocated(By.id(FLD_LOGIN_PASSWORD)), presenceOfElementLocated(By.id(BTN_LOGIN))));

		// Retrieve login fields
		final WebElement usernameEl = chromeDriver.findElement(By.id(FLD_LOGIN_USER_NAME));
		final WebElement passwordEl = chromeDriver.findElement(By.id(FLD_LOGIN_PASSWORD));
		final WebElement loginBtnEl = chromeDriver.findElement(By.id(BTN_LOGIN));

		final Action loginAction = builder.sendKeys(usernameEl, request.getAdminUser())
				.sendKeys(passwordEl, request.getAdminPassword()).click(loginBtnEl).build();

		loginAction.perform();

		wait.until(or(visibilityOfElementLocated(By.id(LBL_LOGIN_ERROR)), urlContains(URL_PRUVAN_DASHBOARD)));

		if (!chromeDriver.getCurrentUrl().toLowerCase().contains(URL_PRUVAN_DASHBOARD)) {
			return new PruvanUserResponse(PruvanUserStatus.FAILED_LOGIN);
		}

		chromeDriver.navigate().to(URL_PRUVAN_USER_MGMT);
		wait.until(urlContains(URL_PRUVAN_USER_MGMT));
		wait.until(and(visibilityOfElementLocated(By.id(FLD_USER_SEARCH))));

		final WebElement searchEl = chromeDriver.findElement(By.id(FLD_USER_SEARCH));
		final Action searchAction = builder.moveToElement(searchEl).pause(500).sendKeys(searchEl, request.getUser())
				.build();

		searchAction.perform();

		wait.until(and(visibilityOfElementLocated(By.id(TBL_USERS)), visibilityOfElementLocated(By.id(BTN_NEW_USER))));

		final List<WebElement> userEls = chromeDriver
				.findElements(By.xpath(String.format("//td[@id=\"username\"][text()='%s']", request.getUser())));

		if (userEls.size() > 0) {
			return new PruvanUserResponse(PruvanUserStatus.FAILED_USER_EXISTS);
		}

		final WebElement newUserEl = chromeDriver.findElement(By.id(BTN_NEW_USER));
		newUserEl.click();

		wait.until(and(visibilityOfElementLocated(By.id(WND_NEW_USER)),
				visibilityOfElementLocated(By.id(FLD_USER_USERNAME)),
				visibilityOfElementLocated(By.id(FLD_USER_PASSWORD)),
				visibilityOfElementLocated(By.id(FLD_USER_FIRST_NAME)),
				visibilityOfElementLocated(By.id(FLD_USER_LAST_NAME)),
				visibilityOfElementLocated(By.id(FLD_USER_EMAIL_ADDRESS)),
				visibilityOfElementLocated(By.id(FLD_USER_ROLE)),
				visibilityOfElementLocated(By.id(FLD_USER_CHECK_IN_NUMBER)),
				visibilityOfElementLocated(By.id(BTN_USER_SAVE))));

		final WebElement wnd = chromeDriver.findElement(By.id(WND_NEW_USER));
		final WebElement wndUsernameEl = chromeDriver.findElement(By.id(FLD_USER_USERNAME));
		final WebElement wndPasswordEl = chromeDriver.findElement(By.id(FLD_USER_PASSWORD));
		final WebElement wndFirstNameEl = chromeDriver.findElement(By.id(FLD_USER_FIRST_NAME));
		final WebElement wndLastNameEl = chromeDriver.findElement(By.id(FLD_USER_LAST_NAME));
		final WebElement wndEmailEl = chromeDriver.findElement(By.id(FLD_USER_EMAIL_ADDRESS));
		final WebElement wndRoleEl = chromeDriver.findElement(By.id(FLD_USER_ROLE));
		final WebElement wndCheckInEl = chromeDriver.findElement(By.id(FLD_USER_CHECK_IN_NUMBER));
		final WebElement wndSaveEl = chromeDriver.findElement(By.id(BTN_USER_SAVE));

		final Action wndAction = builder.sendKeys(wndUsernameEl, request.getUser())
				.sendKeys(wndPasswordEl, request.getPassword()).sendKeys(wndFirstNameEl, request.getFirstName())
				.sendKeys(wndLastNameEl, request.getLastName()).sendKeys(wndEmailEl, request.getEmailAddress())
				.sendKeys(wndCheckInEl, (request.getCheckInNumber() != null) ? request.getCheckInNumber() : "").build();

		final List<WebElement> roleOptions = wndRoleEl.findElements(By.tagName("option"));
		final Map<String, String> roleMap = new HashMap<>();

		for (WebElement el : roleOptions) {
			final String val = el.getAttribute("value");
			final String name = el.getText().toLowerCase();

			if (val != null && !val.isEmpty()) {
				roleMap.put(name, val);
			}
		}

		if (!roleMap.containsKey(request.getRoleName().toLowerCase())) {
			return new PruvanUserResponse(PruvanUserStatus.FAILED_INVALID_ROLE);
		}

		final String roleValue = roleMap.get(request.getRoleName().toLowerCase());
		chromeDriver.executeScript(String.format("$('#roleSelect').val('%s');", roleValue));

		wndAction.perform();
		wndSaveEl.click();

		wait.until(or(not(visibilityOfElementLocated(By.id(WND_NEW_USER))),
				visibilityOfElementLocated(By.id(LBL_USER_ERROR))));

		if (wnd.isDisplayed()) {
			return new PruvanUserResponse(PruvanUserStatus.FAILED);
		}

		return new PruvanUserResponse(PruvanUserStatus.SUCCEEDED);
	}

	@Override
	public void close() throws Exception {
		if (webDriver != null) {
			webDriver.quit();
		}
	}
}
