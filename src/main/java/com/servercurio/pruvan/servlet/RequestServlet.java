package com.servercurio.pruvan.servlet;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.*;
import com.servercurio.pruvan.runtime.LoggingSubsystem;
import com.servercurio.pruvan.runtime.Runtime;
import org.apache.http.HttpHeaders;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class RequestServlet extends HttpServlet {

	private static final Logger log = LoggerFactory.getLogger(RequestServlet.class);
	private ObjectMapper objectMapper;

	public RequestServlet() {
		super();
		this.objectMapper = Runtime.getInstance().getServletSubsystem().getObjectMapper();
	}

	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		super.doGet(req, resp);
	}

	/*
			accepts a JSON input:
			{ "adminUser": "", "adminPassword": "", "user": "", "password": "" }

			returns JSON output:
			{ "status": 1 }
		 */
	@Override
	protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {

		ContentType contentType;

		try {
			contentType = ContentType.parse(req.getHeader(HttpHeaders.CONTENT_TYPE));
		} catch (IllegalArgumentException ex) {
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid Content Type");
			return;
		}

		if (!ContentType.APPLICATION_JSON.getMimeType().equals(contentType.getMimeType())) {
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid Content Type");
			return;
		}

		final InputStream in = req.getInputStream();
		final ObjectReader reader = objectMapper.reader(PruvanUserRequest.class);

		PruvanUserRequest pruvanRequest = null;
		try {
			pruvanRequest = reader.readValue(in);
			in.close();
		} catch (JsonMappingException | JsonParseException ex) {
			log.debug(LoggingSubsystem.LOGM_SERVLET, "Invalid Request Body", ex);
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid Request Body");
		}

		if (pruvanRequest == null || !pruvanRequest.isValid()) {
			resp.sendError(HttpServletResponse.SC_NO_CONTENT, "Invalid JSON Request");
			return;
		}

		final Future<PruvanUserResponse> future = Runtime.getInstance().enqueue(pruvanRequest);

		try {
			writeResponse(resp, future.get());
		} catch (InterruptedException | ExecutionException ex) {
			log.error(LoggingSubsystem.LOGM_EXCEPTION, "Unhandled Execution Error", ex);
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Unhandled Execution Error");
			return;
		}
	}

	@Override
	protected void doPut(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		super.doPut(req, resp);
	}

	@Override
	protected void doDelete(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		super.doDelete(req, resp);
	}

	@Override
	protected void doTrace(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		super.doTrace(req, resp);
	}

	private void writeResponse(final HttpServletResponse resp, final PruvanUserResponse payload) throws IOException {
		final ObjectWriter writer = objectMapper.writer();
		final OutputStream os = resp.getOutputStream();

		resp.setStatus(HttpServletResponse.SC_OK);
		resp.addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
		writer.writeValue(os, payload);
		os.flush();
		os.close();
	}

}
