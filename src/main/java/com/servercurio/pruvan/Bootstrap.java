package com.servercurio.pruvan;

import com.servercurio.pruvan.runtime.LoggingSubsystem;
import com.servercurio.pruvan.runtime.Runtime;

public class Bootstrap {

	public static void main(final String[] args) {
		LoggingSubsystem.getInstance().configure(args);
		Runtime.getInstance().configure(args);

		Runtime.getInstance().start();

		try {
			Runtime.getInstance().join();
		} catch (InterruptedException e) {
			// Suppress the interrupted exception
		}
	}

}
