package com.servercurio.pruvan.servlet;

public class PruvanUserResponse {

	private PruvanUserStatus status;

	public PruvanUserResponse() {

	}

	public PruvanUserResponse(final PruvanUserStatus status) {
		this.status = status;
	}

	public PruvanUserStatus getStatus() {
		return status;
	}

	public void setStatus(final PruvanUserStatus status) {
		this.status = status;
	}
}
