package com.servercurio.pruvan.runtime;

public interface Subsystem {

	void configure(final String[] args);

	void shutdown();

}
