package com.servercurio.pruvan.runtime;

import org.slf4j.*;

public class LoggingSubsystem implements Subsystem {

	public static final Marker LOGM_STARTUP = MarkerFactory.getMarker("STARTUP");
	public static final Marker LOGM_EXCEPTION = MarkerFactory.getMarker("EXCEPTION");
	public static final Marker LOGM_SERVLET = MarkerFactory.getMarker("SERVLET");
	public static final Marker LOGM_SELENIUM = MarkerFactory.getMarker("SELENIUM");

	private static final LoggingSubsystem instance = new LoggingSubsystem();

	private LoggingSubsystem() {

	}

	public static LoggingSubsystem getInstance() {
		return instance;
	}

	@Override
	public void configure(final String[] args) {
		final Logger log = LoggerFactory.getLogger(getClass());

		log.info(LOGM_STARTUP, "Initialized System Logger");
	}

	@Override
	public void shutdown() {
		final Logger log = LoggerFactory.getLogger(getClass());

		log.info(LOGM_STARTUP, "Shutdown System Logger");
	}
}
