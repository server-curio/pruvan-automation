package com.servercurio.pruvan.servlet;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class PruvanUserRequest {

	private String adminUser;
	private String adminPassword;

	private String user;
	private String password;

	private String firstName;
	private String lastName;
	private String emailAddress;

	private String roleName;
	private String checkInNumber;

	public PruvanUserRequest() {
	}

	public PruvanUserRequest(final String adminUser, final String adminPassword, final String user,
			final String password) {
		this.adminUser = adminUser;
		this.adminPassword = adminPassword;
		this.user = user;
		this.password = password;
	}

	public String getAdminUser() {
		return adminUser;
	}

	public void setAdminUser(final String adminUser) {
		this.adminUser = adminUser;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public void setAdminPassword(final String adminPassword) {
		this.adminPassword = adminPassword;
	}

	public String getUser() {
		return user;
	}

	public void setUser(final String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(final String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(final String roleName) {
		this.roleName = roleName;
	}

	public String getCheckInNumber() {
		return checkInNumber;
	}

	public void setCheckInNumber(final String checkInNumber) {
		this.checkInNumber = checkInNumber;
	}

	public boolean isValid() {
		return adminUser != null && !adminUser.isEmpty() &&
				adminPassword != null && !adminPassword.isEmpty() &&
				user != null && !user.isEmpty() &&
				password != null && !password.isEmpty() &&
				firstName != null && !firstName.isEmpty() &&
				lastName != null && !lastName.isEmpty() &&
				emailAddress != null && !emailAddress.isEmpty() &&
				roleName != null && !roleName.isEmpty();
	}
}
