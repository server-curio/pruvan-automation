FROM alpine:edge

RUN apk add --no-cache openjdk8-jre-base openjdk8-jre openjdk8-jre-lib chromium bash coreutils chromium-chromedriver mesa-gl mesa-gles mesa-glapi

WORKDIR /pruvan
COPY ["pruvan-automation.jar", "/pruvan/pruvan-automation.jar"]
RUN mkdir -p /pruvan/driver && cp -rf /usr/bin/chromedriver /pruvan/driver/chromedriver.unix

EXPOSE 8080/tcp

CMD ["/usr/bin/env", "java", "-jar", "pruvan-automation.jar"]
ENTRYPOINT ["/usr/bin/env", "java", "-jar", "pruvan-automation.jar"]

