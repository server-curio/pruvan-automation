package com.servercurio.pruvan.runtime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertySubsystem implements Subsystem {

	private static final Logger log = LoggerFactory.getLogger(SeleniumSubsystem.class);

	public PropertySubsystem() {

	}

	@Override
	public void configure(final String[] args) {
		log.info(LoggingSubsystem.LOGM_STARTUP, "Initialized Properties System");
	}

	@Override
	public void shutdown() {
		log.info(LoggingSubsystem.LOGM_STARTUP, "Shutdown Properties System");
	}

}
