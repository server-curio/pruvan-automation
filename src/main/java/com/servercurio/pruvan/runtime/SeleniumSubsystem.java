package com.servercurio.pruvan.runtime;

import com.servercurio.pruvan.selenium.NewUserAutomation;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.File;
import java.util.Locale;

public class SeleniumSubsystem implements Subsystem {

	private static final Logger log = LoggerFactory.getLogger(SeleniumSubsystem.class);
	private ChromeDriverService driverService;
	private ChromeOptions chromeOptions;
	private OperatingSystem operatingSystem;


	@Override
	public void configure(final String[] args) {
		try {
			determineOsType();

			driverService = new ChromeDriverService.Builder().usingAnyFreePort().withVerbose(false).withSilent(false)
					.usingDriverExecutable(resolveDriverPath()).build();

			chromeOptions = new ChromeOptions();
			chromeOptions
					.setPageLoadStrategy(PageLoadStrategy.NORMAL)
					.setAcceptInsecureCerts(true)
//					.setHeadless(true);
					.setHeadless(GraphicsEnvironment.isHeadless())
					.addArguments("--no-sandbox", "--disable-gpu");

			final File browserPath = resolveBrowserPath();

			if (browserPath != null) {
				chromeOptions.setBinary(browserPath);
			}

			log.info(LoggingSubsystem.LOGM_STARTUP, "Initialized Selenium System");
		} catch (Exception ex) {
			log.error(LoggingSubsystem.LOGM_EXCEPTION, "Error Initializing Selenium System", ex);
		}
	}

	@Override
	public void shutdown() {
		if (driverService != null) {
			if (driverService.isRunning()) {
				driverService.stop();
			}

			driverService = null;
		}

		log.info(LoggingSubsystem.LOGM_STARTUP, "Shutdown Selenium System");
	}

	@SuppressWarnings("unchecked")
	public <T extends WebDriver> T create() {
		return (T) new ChromeDriver(driverService, chromeOptions);
	}

	public OperatingSystem getOperatingSystem() {
		return operatingSystem;
	}

	private void determineOsType() {
		final String osName = System.getProperty("os.name");

		operatingSystem = OperatingSystem.Unknown;

		if (osName == null || osName.isEmpty()) {
			return;
		}

		final String lowerName = osName.toLowerCase();

		if (lowerName.contains("mac") || lowerName.contains("darwin")) {
			operatingSystem = OperatingSystem.MacOS;
		} else if (lowerName.contains("win")) {
			operatingSystem = OperatingSystem.Windows;
		} else if (lowerName.contains("nix") || lowerName.contains("nux")) {
			operatingSystem = OperatingSystem.Unix;
		}

		log.info(LoggingSubsystem.LOGM_SELENIUM, "OS Detection [ os.name = '{}', os.name.toLowerCase() = '{}',  os = '{}' ]", osName, lowerName, operatingSystem.name());
	}

	private File resolveDriverPath() {
		if (OperatingSystem.MacOS.equals(operatingSystem)) {
			return new File("driver/chromedriver.osx");
		} else if (OperatingSystem.Windows.equals(operatingSystem)) {
			return new File("driver/chromedriver.win32.exe");
		} else if (OperatingSystem.Unix.equals(operatingSystem)) {
			return new File("driver/chromedriver.unix");
		}

		return new File("driver/chromedriver");
	}

	private File resolveBrowserPath() {
		if (OperatingSystem.MacOS.equals(operatingSystem)) {
			return new File("browsers/chrome/osx/Chromium.app/Contents/MacOS/Chromium");
		} else if (OperatingSystem.Windows.equals(operatingSystem)) {
			return new File("browsers/chrome/win32/chromium/chromium.exe");
		} else if (OperatingSystem.Unix.equals(operatingSystem)) {
			return new File("/usr/bin/chromium-browser");
		}

		return null;
	}
}
