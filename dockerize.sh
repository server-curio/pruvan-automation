#!/usr/bin/env bash
CWD="$( cd "$( dirname "$0" )" && pwd )"
RM="/usr/bin/env rm"
MKDIR="/usr/bin/env mkdir"
CP="/usr/bin/env cp"
DOCKER="/usr/bin/env docker"
MAVEN="/usr/bin/env mvn"

if [ -d "$CWD/tmp" ]; then
    $RM -rf "$CWD/tmp"
fi

if [ -z "$1" ]; then
    echo
    echo "Usage:  $(basename "$0") <version>"
    echo
    exit 1
fi

$MKDIR -p "$CWD/tmp"

pushd "$CWD"
$MAVEN clean package
popd

$CP "$CWD/target/pruvan-automation-1.0-SNAPSHOT.jar" "$CWD/tmp/pruvan-automation.jar"
$CP "$CWD/Dockerfile" "$CWD/tmp/Dockerfile"

pushd "$CWD/tmp"
$DOCKER image rmi "nathanklick/pruvan:alpine-$1"
$DOCKER build -t "nathanklick/pruvan:alpine-$1" "$CWD/tmp"
popd

$RM -rf "$CWD/tmp"