package com.servercurio.pruvan.runtime;

public enum OperatingSystem {
	Unknown,
	Windows,
	MacOS,
	Unix;
}
