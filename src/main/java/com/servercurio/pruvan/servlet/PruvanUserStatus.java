package com.servercurio.pruvan.servlet;

public enum PruvanUserStatus {
	UNKNOWN,
	SUCCEEDED,
	FAILED,
	FAILED_LOGIN,
	FAILED_USER_EXISTS,
	FAILED_INVALID_ROLE,
	FAILED_EXCEPTION;
}
